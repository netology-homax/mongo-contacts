const express = require('express');
let app = module.exports = express();

let apiV1Contacts = require('./contacts');

app.use('/contacts', apiV1Contacts);
