const express = require('express');
let app = module.exports = express();

const Contact = require('../../../model/Contact');
const Storage = require('../../../storage');

let validFields = ['id', 'name', 'score'];

app.get('/', function(req, res, next) {
  let result = Storage.getContacts()
    .then(contacts => {
      res.json(contacts);
    })
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t get contacts'));
    });
});

app.get('/search', function(req, res, next) {
  let {name, phone} = req.query;
  let search = {};
  if (name) {
    search.name = name;
  }
  if (phone) {
    search.phone = phone;
  }
  let result = Storage.search(search)
    .then(contacts => {
      res.json(contacts);
    })
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t find contacts'));
    });
});

app.post('/', function(req, res, next) {
  let name = req.body.name;
  let phone = req.body.phone;
  let contact = new Contact(name, phone);
  Storage.create(contact)
    .then(id => {
      contact.id = id;
      res.json(contact);
    })
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t add new contact'));
    });
});

app.put('/:id', function(req, res, next) {
  let id = req.params.id;
  let name = req.body.name;
  let phone = req.body.phone;
  let contact = Storage.getContact(id)
    .then(contact => {
      if (!contact) {
        res.status(404);
        res.send('Contact not found');
        return;
      }
      return contact;
    })
    .then(contact => {
      if (contact) {
        contact.name = name || contact.name;
        contact.phone = phone || contact.phone;
        return contact;
      }
    })
    .then(updatedContact => {
      if (updatedContact) {
        return Storage.update(updatedContact);
      }
    })
    .then(contact => res.json(contact))
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t update a contact'));
    });
});

app.delete('/:id', function(req, res, next) {
  let id = req.params.id;
  let contact = Storage.getContact(id)
    .then(contact => {
      if (!contact) {
        res.status(404);
        res.send('Contact not found');
        return;
      }
      return contact;
    })
    .then(contact => {
      if (contact) {
        return Storage.delete(contact);
      }
    })
    .then(contact => res.json(contact))
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t delete a contact'));
    });
});
