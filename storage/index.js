const Contact = require('../model/Contact');
const MongoStorage = require('./MongoStorage');

class ContactMongoStorage extends MongoStorage {
  create(contact) {
    if (contact instanceof Contact) {
        return super.createOne({
          name: contact.name,
          phone: contact.phone
        })
        .then(result => {
          return result.insertedId;
        })
        .catch(error => {
          throw error;
        });
    }
  }

  search(data) {
    return super.search(data)
      .then(contacts => contacts.map(contact => ({
        id: contact._id,
        name: contact.name,
        phone: contact.phone
      })))
      .catch(error => {
        throw error;
      });
  }

  getContacts() {
    return super.get()
      .then(contacts => contacts.map(contact => ({
        id: contact._id,
        name: contact.name,
        phone: contact.phone
      })))
      .catch(error => {
        throw error;
      });
  }

  getContact(id) {
    return super.getOne(id)
      .then(contact => {
        if (contact) {
          let cont =  new Contact(contact.name, contact.phone);
          cont.id = contact._id;
          return cont;
        }
        return null;
      })
      .catch(error => {
        throw error;
      });
  }

  update(contact) {
    if (contact instanceof Contact) {
        return super.updateOne(contact.id, {
          name: contact.name,
          phone: contact.phone
        })
        .then(result => {
          return contact;
        })
        .catch(error => {
          throw error;
        });
    } else {
      throw new Error('Invalid object type, expected Contact');
    }
  }

  delete(contact) {
    if (contact instanceof Contact) {
        return super.deleteOne(contact.id)
          .then(result => {
            return contact;
          })
          .catch(error => {
            throw error;
          });
    } else {
      throw new Error('Invalid object type, expected Contact');
    }
  }
}

module.exports = new ContactMongoStorage();
