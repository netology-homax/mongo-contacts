let mongo = require('mongodb');
let MongoClient = mongo.MongoClient;
const config = require('../config');

class MongoStorage {
  constructor() {
    let self = this;
    this.collection = config.mongodb.collection;
    MongoClient.connect(config.mongodb.connect, function(err, db) {
      if (err) {
        throw err;
      }
      self.db = db;
    });
  }

  getOne(id) {
    let o_id = new mongo.ObjectID(id);
    return new Promise((resolve, reject) => {
      let collection = this.db.collection(this.collection);
      collection.findOne({_id: o_id}, (err, contact) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(contact);
      });
    });
  }

  get() {
    return new Promise((resolve, reject) => {
      let collection = this.db.collection(this.collection);
      collection.find().toArray((err, contacts) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(contacts);
      });
    });
  }

  search(data) {
    return new Promise((resolve, reject) => {
      let collection = this.db.collection(this.collection);
      let criteria = {};
      if (data.name) {
        criteria.name = {$regex: data.name};
      }
      if (data.phone) {
        criteria.phone = {$regex: data.phone};
      }
      collection.find(criteria).toArray((err, contacts) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(contacts);
      });
    });
  }

  createOne(data) {
    return new Promise((resolve, reject) => {
        let collection = this.db.collection(this.collection);
        collection.insertOne(data, (err, result) => {
            if (err) {
              reject(err);
              return;
            }
            if (result.result.n !== 1) {
              reject('Not all records inserted');
              return;
            }
            if (result.ops.length !== 1) {
              reject('Wrong operation count');
              return;
            }
            resolve(result);
        });
    });
  }

  updateOne(id, data) {
    return new Promise((resolve, reject) => {
        let collection = this.db.collection(this.collection);
        collection.updateOne({_id: id}, data, (err, result) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(result);
        });
    });
  }

  deleteOne(id) {
    return new Promise((resolve, reject) => {
        let collection = this.db.collection(this.collection);
        collection.deleteOne({_id: id}, (err, result) => {
            if (err) {
              reject(err);
              return;
            }
            resolve(result);
        });
    });
  }
}

module.exports = MongoStorage;
