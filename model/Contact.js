class Contact {
  constructor(name, phone) {
    this.name = name || 'Mr X';
    this.phone = phone || '911';
  }
}

module.exports = Contact;
